/**
 * @format
 */
import React from 'react';
<<<<<<< HEAD
import { AppRegistry } from 'react-native';
import App from './src/App';
import { appId } from './app.json';
import MiniApi from '@momo-miniapp/api';
import { ApplicationStyle } from '@momo-platform/component-kits';
import { Provider } from 'react-redux';
import configureStore from './src/redux/reducer';

const store = configureStore()

ApplicationStyle();


class MiniApp extends React.Component {
    constructor(props) {
        super(props);
        /**
         * do not remove code bellow
         */
        MiniApi.init(props);
    }

    render() {
        return <Provider store={store}>
            <App {...this.props} />
        </Provider>;
    }
}

AppRegistry.registerComponent(appId, () => MiniApp);
AppRegistry.registerComponent('MiniApp', () => MiniApp);
=======
import App from './src/Application';
import AppJson from './app.json';
import { ApplicationStyle } from '@momo-kits/core';
import MiniApi from '@momo-miniapp/api';
ApplicationStyle();

export const MiniApp = props => {
    return <App {...props} />;
};

/**
 * do not change codes bellow
 */
if (__DEV__) {
    AppJson.bridgeMode = 2;
}

MiniApi.registerApp(AppJson, () => MiniApp);
>>>>>>> master-v3
