
export const MESSAGE_ACTION = {
    CHANGE: 'CHANGE',
};

export function changeMessageAction(message) {
    return {
        type: MESSAGE_ACTION.CHANGE,
        payload: message
    }
}