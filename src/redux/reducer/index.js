import { createStore, combineReducers } from 'redux';
import messageReducer from './messageReducer';

const rootReducer = combineReducers(
    { message: messageReducer }
);
const configureStore = () => {
    return createStore(rootReducer);
}
export default configureStore;