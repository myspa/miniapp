import axiosClient from "./axios";
class PaymentApi{
    postNoti=(props)=>{
        // console.log('message api',props);
        const url ='/paymentgateways/momo/notify';
        const transactionUuid = props.requestId;
        const transactionUrl = 'paymentgateways'+transactionUuid+'status';
        return axiosClient.post(url,...props);
    }
}
const paymentApi = new PaymentApi();
export default paymentApi;