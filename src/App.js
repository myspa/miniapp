/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { Navigation } from '@momo-platform/component-kits';
import appJson from '../app.json';
import MiniAppTest from './screens/example/MainScreen';
// import * as Sentry from "@sentry/react";
// import { Integrations } from "@sentry/tracing";

// Sentry.init({
//   dsn: "https://bd1f971a60d34bfab10b7aebed75da69@o1108259.ingest.sentry.io/6135796",
//   integrations: [new Integrations.BrowserTracing()],

//   // We recommend adjusting this value in production, or using tracesSampler
//   // for finer control
//   tracesSampleRate: 1.0,
// });
export default class MiniAppStack extends React.Component {
    render() {
        const {
            params = {},
            options = { title: appJson.displayName }
        } = this.props;
        return <Navigation
            screen={MiniAppTest}
            params={params}
            options={options} />;
    }
}
