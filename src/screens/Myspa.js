import React, { useState, Component, useLayoutEffect, useEffect } from 'react';
import { View, Image } from 'react-native';
import app from '../../app.json';
// import{ WebView} from 'react-native-webview';
import { Text, NavigationBar,
  WebView, Spacing, Button } from '@momo-kits/core';
import Miniapi from '@momo-miniapp/api';
// import * as Sentry from "@sentry/react";
function Myspa({ navigator, params }){
    console.log(params)
    // const base_url = 'http://devmomo.myspa.vn/Frontend';
    const base_url = 'http://momo.myspa.vn/Frontend';
    // const base_url = 'https://tiki.vn/';
    // const base_url = 'http://192.168.1.128:3000/Frontend/'; 
    // const base_url = 'http://192.168.1.171:3000/Frontend/'; 

    const [user, setUser] = useState({'name':'','phone':'','email':''});
    const [url, setUrl] = useState('');
    // scope.setTag ("miniApp", error);
    // const obj = WebView;
    useLayoutEffect(() => {
        navigator.setOptions({
            title: '',
            headerLeft: () => (
                <View style={{flexDirection: 'row', alignItems: 'center',marginLeft: 20}}>
                  <Image 
                    source={{uri:"https://momo.myspa.vn/favicon.ico"}} 
                    // source={{uri:"https://devmomo.myspa.vn/static/media/beautyX.661b1947.svg"}}
                    style={{width: 30, height: 30, borderRadius: 30}}
                    
                  />
                  <Text
                    style={{color: 'white', marginLeft: 8, fontSize: 15}}
                    onPress={()=>setUrl(`${base_url}?email=${(user?.email)||''}&telephone=${user?.phone}&name=${user?.name}&momo=${true}}`)}
                  >
                    {app.displayName}
                  </Text>
                </View>
            ),
            headerTintColor: 'white',
            headerBackground: (headerProps) => {
              return(
                  <NavigationBar
                      {...headerProps}
                      colors={['#7161ba','#B5A5FF']}
                  />
              )},
            });
        });
        // const scope = new Sentry.Scope();
        function getUserInfo(){
          Miniapi.requestUserConsents(
            {
              permissions: [
                { role: "phone",require: true },
                { role: "name" },
                { role: "email",require: true}
              ],
            },
            (a = {}) =>  {
              // return user's information
              try {
                console.log('a',a);
                setUser(a);
                if(url && url!=='' && a.phone && a.email){
                  return true;                  
                }
                else{
                  if(a.phone && params.requestId){
                    setUrl(`${base_url}/Momo-layout-payment/success?email=${(a?.email)||''}&telephone=${a.phone}&name=${a.name}&momo=${true}&requestId=${params.requestId}`)
                  } 
                  else if(a.phone && params.deeplink) {
                    setUrl(`${params.deeplink}?email=${(a?.email)||''}&telephone=${a?.phone}&name=${a?.name}&momo=${true}`);
                  }
                  else if(a.phone) { 
                    Miniapi.setItem('user',a);
                    setUrl(`${base_url}?email=${(a?.email)||''}&telephone=${a?.phone}&name=${a?.name}&momo=${true}`);
                  }
                  else {
                    setUrl('');
                    return false
                  }
                }
              } catch (error) {
                // Sentry.captureException(new Error("something went wrong in mini app"), () => scope);
                console.log(err)
              }
            }
          );
          Miniapi.requestPermission(
            'camera', (data) => {
              console.log(data);
            },
            'photo', (data) => {
              console.log(data);
            },
            'location', (data) => {
              console.log(data);
            }
          )
            // Miniapi.getUserConsents({
            //     "permissions": [
            //         {
            //             "role": "phone",
            //         },{
            //             "role": "email"
            //         }
            //     ]
            // }, (data) => {
            //     console.log(data)
            // })
        }
        // function getStorage(){
        //     console.log('object' ,'out')
        //     Miniapi.getItem('user',(e)=>{
        //         if(e && e.phone){
        //             console.log('e ',e);
        //             setUser(e);
                    // if(params.requestId){
                    //     console.log('object')
                    //   // setOff(false);
                    //   setUrl(`${base_url}/Momo-layout-payment/success?email=${(e?.email)||''}&telephone=${e.phone}&name=${e.name}&momo=${true}&requestId=${params.requestId}`)
                    // } 
                    // else if(e && e.phone) {
                    //   console.log('e inside',e);
                    //   // setOff(false);
                    //   setUrl(`${base_url}?email=${(e?.email)||''}&telephone=${e?.phone}&name=${e?.name}&momo=${true}`)
                    // }
        //         }
        //         else{
        //         // setOff(true);
        //         getUserInfo();
        //         }
        //     });
        // }
        // const injectedJs = `window.postMessage("ALO");`;
        useEffect( ()=>{
            // getStorage();
            getUserInfo();
            const onMiniAppFocusSub = Miniapi.listen('onFocusApp', async (e) => { 
              getUserInfo();
              console.log(e) 
            });
            return () => {
              onMiniAppFocusSub.remove();
            };
        }, [url]);
            // console.log('on: ',on);
            console.log('url: ',url);
        return(
          url?
            <WebView
              // incognito={true}
              // originWhitelist={[‘*’]}
              source={{ uri: url}}
              useWebKit={true}
              allowsInlineMediaPlayback={true}
            />
            :

            <View style={{  padding: Spacing.M, backgroundColor: 'white', display: 'flex', justifyContent: 'center', flexDirection: 'column', height: '100%'}}>
            <Text.H3 style={{textAlign: 'center'}}>Welcome to Myspa App</Text.H3>
            <Text.H4 style={{ marginTop: Spacing.L , fontWeight: 'bold', textAlign: 'center'}}>Myspa warning</Text.H4>
            <View style={{  textAlign: 'center',paddingVertical: Spacing.L }}>
                <Text style={{  textAlign: 'center',}}> Myspa cần truy cập số điện thoại & Họ và tên của bạn để phục vụ tốt nhất
                  
                </Text>
                <Button  style={{ marginTop: Spacing.L }} buttonStyle={{backgroundColor: "#7161BA"}}  onPress={()=>getUserInfo()} title="Cung cấp" />
            </View>
            </View>
        );
}
export default Myspa;